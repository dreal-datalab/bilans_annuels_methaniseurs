# Exprt DS méthaniseurs

load("~/Travail/R_local/enr_reseaux_teo/collecte/registre_biogaz/registre_biogaz_2020_r52.RData")
library(tidyverse)
library(sf)
b <- unnest(inst_biogaz_reg, cols  = c(data_annuelles)) %>%  filter(annee == 2020) %>% 
  st_drop_geometry() %>% select(-contains("_DE_L_EPCI"))

write.csv2(b, file = "../biogaz/exploitation_enquete/prod_inst_injection_bioch4_2020.csv")

